package com.empresa.myapp.myapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.example.Operacao;

@SpringBootApplication
public class MyappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyappApplication.class, args);

		try (
			Operacao h = new Operacao();
		) {
			System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
			System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
			System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
			MyappApplication.quadradoFormatado(2);
		}
	}

	public static void quadradoFormatado(float x) {
		Operacao op = new Operacao();
		float ok = op.quadrado(2);
		System.out.println("O quadrado de "+x+"eh: "+ok);
	}
}
